package com.codistan.java.tasks;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Task031 {
	public static void main(String[] args) {
		BufferedReader br = null;
		String line;

		try {

			br = new BufferedReader(new FileReader("C:\\testing.txt"));
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

}
