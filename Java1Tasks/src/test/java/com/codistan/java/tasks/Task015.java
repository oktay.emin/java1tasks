package com.codistan.java.tasks;

//Super or parent class 
class Calculator {
	public int add(int a, int b) {
		return a + b;
	}
}

//Subclass or Child Class
class CalcAdv extends Calculator {
	public int sub(int a, int b) {
		return a - b;
	}
}

public class Task015 {

	public static void main(String[] args) {
		CalcAdv Calculation = new CalcAdv();
		int result = Calculation.add(5, 6);
		int result1 = Calculation.sub(14, 7);
		System.out.println(result);
		System.out.println(result1);

	}

}