package com.codistan.java.tasks;

//SuperClass or Parent Class
class Encapsulate 
{ 
    private String DriverName; 
    private int DriverRoll; 
    private int DriverAge; 
  
    public int getAge()  
    { 
      return DriverAge; 
    } 
   
    public String getName()  
    { 
      return DriverName; 
    } 
      
    public int getRoll()  
    { 
       return DriverRoll; 
    } 
    
    public void setAge( int newAge) 
    { 
      DriverAge = newAge; 
    } 
   
    public void setName(String newName) 
    { 
      DriverName = newName; 
    } 
       
    public void setRoll( int newRoll)  
    { 
      DriverRoll = newRoll; 
    } 
} 


//Subclass or Child class
public class Task017 {

	public static void main (String[] args)  
    { 
        Encapsulate obj = new Encapsulate(); 
           
        obj.setName("Harsh"); 
        obj.setAge(19); 
        obj.setRoll(51); 
          
        System.out.println("Driver's name: " + obj.getName()); 
        System.out.println("Driver's age: " + obj.getAge()); 
        System.out.println("Driver's roll: " + obj.getRoll()); 
                  
    } 
} 
	

