package com.codistan.java.tasks;

public class Task012 {
	static void BlockA() {
		try {
			System.out.println("inside BlockA");
			throw new RuntimeException("demo");
		} finally {
			System.out.println("BlockA's finally");
		}
	}

	static void BlockB() {
		try {
			System.out.println("inside BlockB");
			return;
		} finally {
			System.out.println("BlockB's finally");
		}
	}

	public static void main(String args[]) {
		try {
			BlockA();
		} catch (Exception e) {
			System.out.println("Exception caught");
		}
		BlockB();
	}
}
