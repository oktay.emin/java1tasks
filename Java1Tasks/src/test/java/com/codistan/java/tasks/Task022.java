package com.codistan.java.tasks;
import java.util.TreeSet;

import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;

public class Task022 {
	 
	
	
	public static void main(String[] args) {
		TreeSet<String> ts = new TreeSet<String>();
		ts.add("Bulent");
		ts.add("Suleyman");
		ts.add("Cevdet");
		ts.add("Gokhan");
		ts.add("Oktay");
		ts.add("Admin");
		
		//This will not added since Admin has been added previously.
		ts.add("Admin");
		
		System.out.println(ts);
	}
}



