package com.codistan.java.tasks;

class Animal {
	public void sound() {
		System.out.println("Animal sound");
	}
}

public class Task016 extends Animal {
	public static void main(String[] args) {
		Animal obj = new Task016();
		obj.sound();

	}
}
