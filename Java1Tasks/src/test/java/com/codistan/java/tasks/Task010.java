package com.codistan.java.tasks;

public class Task010 {
	
	  protected String fname = "John";
	  protected String lname = "Doe";
	  protected String email = "john@doe.com";
	  protected int age = 24;

	  public static void main(String[] args) {
	    Task010 myObj = new Task010();
	    System.out.println("Name: " + myObj.fname + " " + myObj.lname);
	    System.out.println("Email: " + myObj.email);
	    System.out.println("Age: " + myObj.age);
	  }
	}
	
	

