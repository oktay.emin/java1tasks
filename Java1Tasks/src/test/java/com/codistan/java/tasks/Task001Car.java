package com.codistan.java.tasks;

public class Task001Car{

	public String color;
	protected String car;
	String horsePower;
	private double engine;

	public Task001Car(String car, String color, String horsePower, double engine) {
		this.car = car;
		this.color = color;
		this.horsePower = horsePower;
		this.engine = engine;
	} 
	public void driveCar() {
		//System.out.println("Driving:" + this.car);
	}
	
	public double getEngine() {
		return engine;
	}
	public void setEngine(double engine) {
		this.engine = engine;
	}
	
	
}
