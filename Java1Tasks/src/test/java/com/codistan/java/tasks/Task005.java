package com.codistan.java.tasks;

public class Task005 {
	void show() {
		System.out.println("Parent's show()");
	}
}

class Child extends Task005 {

	@Override
	void show() {
		super.show();
		System.out.println("Child's show()");
	}
}

class Main {
	public static void main(String[] args) {
		Task005 obj = new Child();
		obj.show();

	}
}