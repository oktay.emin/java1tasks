package com.codistan.java.tasks;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

public class Task002 {
	@Test
	public void PI() {
		// Final Keyword
		final double PI = 3.141592653589793;
		System.out.println(PI);

		// Finally Keyword
		try {
			int data = 25 / 0;
			System.out.println(data);
		} catch (NullPointerException e) {
			System.out.println(e);
		} finally {
			System.out.println("finally block is always executed");
		}
		System.out.println("rest of the code...");
	}

}
