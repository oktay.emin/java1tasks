package com.codistan.java.tasks;

public class Task006 {
	public static void main(String[] args) {
		Test obj = new Test(6.0);
	}
}

class Test {
	public Test() {
		System.out.println("In Test Const");
	}

	public Test(double i) {
		System.out.println("In Test Const parameter");
	}

}
