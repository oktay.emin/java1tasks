package com.codistan.java.tasks;

public class Task001 {
	public static void main(String[] args) {

		//TC001
		Task001Car mustang = new Task001Car("Mustang", "Red", "500hp", 6.0);		
		// Default Access Modifier
		System.out.println(mustang.horsePower);

		// Public Access Modifier
		System.out.println(mustang.color);

		// Protected Access Modifier
		System.out.println(mustang.car);
		
		//Private Access Modified
		System.out.println(mustang.getEngine());

		
		
		
		
	}

}
