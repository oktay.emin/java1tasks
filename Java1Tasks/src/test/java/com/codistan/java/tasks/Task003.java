package com.codistan.java.tasks;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

public class Task003 {

	@Test
	public void myMethod() {
		System.out.println("Overridden method");
	}
}

class Demo extends Task003 {
	public void myMethod() {
		// This will call the myMethod() of parent class
		super.myMethod();
		System.out.println("Overriding method");
	}

	public static void main(String args[]) {
		Demo obj = new Demo();
		obj.myMethod();
	}
}