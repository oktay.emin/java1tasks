package com.codistan.java.tasks;

import java.util.Arrays;

public class Task028 {
	public static void main(String[] args) {
		int arry[] = { 2, 1, 9, 6, 4 };
		for (int number : arry) {
			System.out.println("Number = " + number);
		}
		Arrays.sort(arry);
		System.out.println("The sorted int array is:");
		for (int number : arry) {
			System.out.println("Number = " + number);
		}
	}
}
